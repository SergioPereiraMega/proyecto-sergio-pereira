<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Car;
use App\BrandModel;
use Illuminate\Support\Facades\Auth;

class AdminCarController extends Controller
{

    public function index()
    {
      $cars = Car::all();
      return view ('/admin/carList', ['cars'=>$cars]);
    }

    public function create(Request $request)
    {
      $model = BrandModel::all();
      return view ('/admin/carCreateForm', ['model'=>$model]);
    }

    public function store(Request $request)
    {
      $car = new Car;
      $car->year = $request->get('year');
      $car->model_id = $request->get('model_id');
      $car->description = $request->get('description');
      $car->price = $request->get('price');
      $car->photo = "";
      // Car::create($car);
      $car->save();
      return redirect('/admin/autos/crear')->with('mensaje', 'El auto se creo correctamente');
    }


    public function edit($id)
    {
      $car = Car::find($id);
      return view ('admin.carEditForm', ['car'=>$car]);

    }

    public function update(Request $request, $id)
    {
      $car = Car::find($id);


    }

    public function delete($id)
    {

    }
}
