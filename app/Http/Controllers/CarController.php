<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Car;
use App\Brand;
use App\BrandModel;


class CarController extends Controller
{
    public function sales(Request $request)
    {
      $models = BrandModel::all();
      $brands = Brand::all();
      $cars = Car::search($request->all())->paginate(7);
      return view ('web.sales', ['cars'=>$cars, 'models'=>$models, 'brands'=>$brands]);

    }

    public function showReserve(Request $request, $id)
    {
        # code...
    }

    public function reserve(Request $request, $id)
    {
        # code...
    }


    public function listJson(Request $request)
    {
        # code...
    }

    public function detailJson($id)
    {
        # code...
    }
}
