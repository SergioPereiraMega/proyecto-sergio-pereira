<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Car;
use App\BrandModel;

class PagesController extends Controller
{

    public function index()
    {
      return view ('web.home');
    }

    public function aboutUs()
    {
      return view ('web.aboutUs');
    }

    public function sendContact()
    {
      return view ('sections.contactModel');
    }


}
