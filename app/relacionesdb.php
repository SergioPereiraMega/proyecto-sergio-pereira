<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['name'];
}
// ----------------------------------------
class BrandModel extends Model
{
    protected $fillable = ['name', 'brand_id'];

    protected $table = 'models';

    public function brand(){
    	return $this->belongsTo(Brand::class);
    }

}
// ----------------------------------------
class Car extends Model
{
  public function model() {
    return $this->belongsTo(BrandModel::class);
  }
}
