<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
  public function model() {
    return $this->belongsTo(BrandModel::class);
  }
  public static function search($inputs = [])
  {
    $query = new Car;
    if(isset($inputs['year'])) {
      $query = $query->where('year', '=', $inputs['year']);
    }
    if (isset($inputs['brand_id'])) {
      $brandId = $inputs['brand_id'];
      $query = $query->whereHas('model', function ($query) use($brandId) {
      $query->where('brand_id', $brandId);
      });
    }
    if(isset($inputs['model_id'])) {
      $query = $query->where('model_id', '=', $inputs['model_id']);
    }
    if(isset($inputs['q'])) {
      $query = $query->where('description', 'like', '%'.$inputs['q'].'%');
    }
    return $query;
  }
}
