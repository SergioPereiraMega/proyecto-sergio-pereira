@extends('layouts.master')

@section('title')
Ventas
@endsection

@section('content')

<div id="sales">
	<div class="container">
		<div class="row">
            <div class="col-xs-12">
                <h1>Ventas</h1>
            </div>
        </div>
		<hr>
		<div class="row">
			<div class="col-md-2">
				<div id="filter">
					<h4>Filtro</h4>

          <div class="form-group">
  					<form method="GET" action="/ventas">
  						<div class="form-group">
  							<label for="year">Año</label>
  							<select id="select-year" class="form-control input-sm" name="year">
  								<option value="year">Seleccionar...</option>
  								@for ($i = 2017; $i >= 1980; $i--)
  								<option value="{{$i}}">{{ $i }}</option>
  								@endfor
  							</select>
  						</div>

							<label for="brand">Marca</label>
							<select id="select-brand" class="form-control input-sm" name="brand_id">
                <option value="">Seleccionar...</option>
                @foreach ( $brands as $brand )
                <option value="">{{ $brand->name }}</option>
                @endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="model">Modelo</label>
							<select id="select-model" class="form-control input-sm" name="model_id">
                <option value="model_id">Seleccionar...</option>
                @foreach ( $models as $model )
                <option value="">{{ $model->name }}</option>
                @endforeach
              </select>
						</div>
            <div class="form-group">
                <label for="model">Texto</label>
                <input id="text-search" class="form-control input-sm" name="q">
            </div>

						<button id="btn-filter" type="submit" name="button" class="btn btn-warning btn-sm btn-block"><i class="fa fa-search" aria-hidden="true"></i> Filtrar</button>

					</form>



				</div><!-- /#filter -->

			</div><!-- /.col -->

			<div class="col-md-10">

				<div class="alert alert-warning hidden" role="alert">
					Lo sentimos, no hay autos para el criterio de búsqueda seleccionado.
				</div>

        @foreach ($cars as $car)
				<div class="car" >
					<img src="{{ $car->photo }}" />
					<h3>
						{{ $car->model->brand->name }} {{ $car->model->name }}
						<span >
							(USD {{ $car->price }})
						</span>
					</h3>
          <p>
            {{ $car->description }}
          </p>
					<div class="car-footer">
						<a type="button" name="button" class="btn btn-success btn-sm"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Reservar</a>
						<a type="button" name="button" class="btn btn-default btn-sm">Más información</a>
					</div>
				</div><!-- /.car -->
        @endforeach
        {{ $cars->appends([])->links() }}



			</div><!-- /.col -->

		</div><!-- /.row -->

	</div><!-- /.container -->
</div><!-- /#sales -->

@endsection
